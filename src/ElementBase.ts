/**
 * 元素类型
 */
export enum ELEMENT_TYPE {
    PATH,
    DRAW,
    TEXT,
}
/**
 * 绘制点
 */
export class Point {
    public X: number = 0
    public Y: number = 0
    constructor(x: number = 0, y: number = 0) {
        this.X = x
        this.Y = y
    }
}
/**
 * 绘制元件
 */
export class ElementBase {
    public Type: ELEMENT_TYPE = ELEMENT_TYPE.DRAW
    public Paths: Array<Point> = []
    public Stroke:string = 'rgba(0,0,0,255)'
    public Fill: string = 'rgba(0,0,0,0)'
    public Data: any = null
    public Position: Point = new Point()
    public Scale:number = 1
    public Rotate:number = 0
}