/**
 * 图层类型
 */
export enum LAYER_TYPE {
    NORMAL,
    TEXT,
}
/**
 * 图层
 * 数据暂时不分离，全部以数组保存
 * 如果改成单个元件，历史数据那边也得改
 */
export class Layer {
    /**
     * 类型
     */
    public Type: LAYER_TYPE = LAYER_TYPE.NORMAL
    private data:any
    /**
     * 是否显示
     */
    public Show:boolean = true
    /**
     * 位置
     */
    public Position = {
        X:0,
        Y:0
    }
    /**
     * 缩放
     */
    public Scale:number = 1
}