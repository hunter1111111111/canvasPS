import { Config, History } from "./Config";
import { Layer } from "./Layer";
/**
 * 图片编辑类
 */
class CanvasPs {
    private c: any
    private ct: any;
    private n_data: any;//新数据
    private o_data: any;//原始数据
    private data: any;//直接数据，保存可用
    private alw = 0;//容差
    private animation: any;//选区视效
    private sample: any;//采样点
    private select = false;//当前选择采样点
    private select_type = 0;//0-颜色选择 1-多边形
    private selectdata: any;//当前选择数据
    private imgsrc = null;//临时存在img对象
    private img: any
    private lastmode = 0;//上次使用的模式
    private polygoncallback: any = null;//多边形回调
    private pickercallback: any = null;//拾色器回调
    //图层集合
    public Layers: Layer[] = []
    private config = Config
    /**
     * 画布
     */
    get Canvas() {
        return this.c
    }
    /**
     * 画布内容
     */
    get Context() {
        return this.ct
    }
    constructor(canvas: any) {
        this.c = canvas
        this.ct = this.c.getContext('2d', { willReadFrequently: true });
        this.config.canvas.x = this.c.offsetX
        this.config.canvas.y = this.c.offsetY
        this.addHistory(new History('开始', this.data));
        this.config.history.List[0].data = this.ct.getImageData(0, 0, this.c.width, this.c.height)
        this.ini_events()
    }
    /**
     * 初始化事件
     */
    ini_events() {
        this.c.style.position = 'absolute';
        //-----画板----鼠标交互
        this.c.onmousedown = (e: any) => {
            e.preventDefault();
            this.config.mouse.status = 0;
            this.config.canvas.x = e.pageX - this.c.offsetLeft;
            this.config.canvas.y = e.pageY - this.c.offsetTop;
            this.config.mouse.down.x = e.pageX - 80;
            this.config.mouse.down.y = e.pageY;
            if (this.config.mode == 4) {
                this.showCuttingBox(true);
            } else if (this.config.mode == 9) {
                this.ct.lineCap = 'round';
                this.ct.strokeStyle = this.config.pen.color;
                this.ct.lineWidth = this.config.pen.size;
                this.ct.beginPath();
            } else if (this.config.mode == 10) {
                this.ct.lineCap = 'round';
                this.ct.strokeStyle = this.config.pen.color;
                this.ct.lineWidth = 0.1;
                this.ct.beginPath();
                this.imgsrc = this.c.toDataURL();
            }
            this.polygon(e.offsetX / (this.config.canvas.scale / 100), e.offsetY / (this.config.canvas.scale / 100), e.which);
            // console.log(e.which)
        }
        this.c.onmouseup = (e: any) => {
            this.config.mouse.status = 1;
            this.config.mouse.cutBox.status = 1;
            switch (this.config.mode) {
                case 0:
                    break;
                case 1:
                    break;
                case 2:
                    this.upData();
                    this.addHistory(new History('笔刷', this.data))
                    break;
                case 3:
                    this.upData();
                    this.addHistory(new History('擦除', this.data))
                    break;
                case 4:

                case 5:

                    break;
                case 6:

                    break;
                case 7:

                    break;
                case 8:

                    break;
                case 9:
                    this.upData();
                    this.addHistory(new History('铅笔', this.data))
                    break;
                case 10:
                    this.pencutdata();//保存抠图结果
                    this.upData();
                    this.addHistory(new History('抠图', this.data))
                    break;
                case 12:
                    if (e.which === 3) {
                        if (this.pickercallback) {
                            this.pickercallback(true);
                        }
                    }
                    break;
            }
        }
        this.c.oncontextmenu = (e: any) => {
            e.preventDefault();
        }
        this.c.onmousemove = (e: any) => {
            switch (this.config.mode) {
                case 0:
                    break;
                case 1:
                    break;
                case 2:
                    this.mouseWork.draw(e.layerX, e.layerY);
                    break;
                case 3:
                    this.mouseWork.clear(e.layerX, e.layerY);
                    break;
                case 4:
                    this.mouseWork.cutting(e.layerX + parseInt(this.c.style.left), e.layerY + parseInt(this.c.style.top));
                    break;
                case 5:
                    // this.mouseWork.cutting(e.layerX,e.layerY,this);
                    break;
                case 6:
                    this.mouseWork.moveCanvas(e.pageX - this.config.canvas.x, e.pageY - this.config.canvas.y);
                    break;
                case 7:
                    break;
                case 8:
                    break;
                case 9:
                    this.mouseWork.pencil(e.layerX, e.layerY);
                    break;
                case 10:
                    this.mouseWork.pencut(e.layerX, e.layerY);
                    break;
                case 12:
                    this.mouseWork.pickercolor(e.layerX - parseInt(this.c.style.left), e.layerY - parseInt(this.c.style.top));
                    break;
            }
        }
        this.c.ondblclick = (e: any) => {
            this.wand(e.offsetX / (this.config.canvas.scale / 100), e.offsetY / (this.config.canvas.scale / 100));
        }
        this.ready();
        this.upData();
    }

    //初始化
    ready() {
        this.c.style.width = null;
        this.c.style.height = null;
        this.c.style.left = 0;
        this.c.style.top = 0;
        this.data = this.ct.getImageData(0, 0, this.c.width, this.c.height);
        this.n_data = this.ct.getImageData(0, 0, this.c.width, this.c.height);
        this.o_data = this.ct.getImageData(0, 0, this.c.width, this.c.height);
        this.config.history.List[0].data = this.ct.getImageData(0, 0, this.c.width, this.c.height);
    }
    //设置模式
    setMode(mode: number, callback: any) {
        if (mode === 11) {
            this.lastmode = mode;
        } else {
            this.lastmode = this.config.mode * 1;
        }

        this.config.mode = mode;

        switch (mode) {
            case 4:
                this.c.style.cursor = "crosshair";
                this.showCuttingBox(true);
                break;
            case 6:
                this.c.style.cursor = "move";
                break;
            case 11:
                this.c.style.cursor = "default";
                this.ct.beginPath();//开启多边形
                this.ct.fillStyle = this.config.pen.color;
                this.ct.strokeStyle = this.config.pen.color;
                this.ct.lineWidth = this.config.pen.size / 10;
                break;
            case 12:
                this.c.style.cursor = "crosshair";
                break;
            default:
                this.c.style.cursor = "default";
                break;
        }
        if (mode != 4) {
            this.showCuttingBox(false);
        }
        if (mode != 8) {
            this.stopAnimate();
        }
        if (callback) {
            this.polygoncallback = callback;
        }
    }
    //根据数据重绘图像
    redraw(data: any) {
        const img = new Image();
        const canvas = document.createElement('canvas');
        if (canvas == null) {
            return
        }
        canvas.width = this.c.width;
        canvas.height = this.c.height;
        var ct = canvas.getContext('2d');
        if (ct == null) {
            return
        }
        ct.putImageData(data, 0, 0);
        img.src = canvas.toDataURL();
        img.onload = () => {
            this.ct.drawImage(img, 0, 0);
            this.selectdata = this.ct.getImageData(0, 0, this.c.width, this.c.height);
        }
    };
    //获取选区内数据
    getareadata(data: any) {
        var img = new Image();
        var canvas = document.createElement('canvas');
        canvas.width = this.c.width;
        canvas.height = this.c.height;
        var ct = canvas.getContext('2d');
        if (ct == null) {
            return
        }
        ct.putImageData(data, 0, 0);
        img.src = canvas.toDataURL();
        img.onload = () => {
            this.ct.drawImage(img, 0, 0);
            this.selectdata = this.ct.getImageData(0, 0, this.c.width, this.c.height);
            this.ct.putImageData(data, 0, 0);
            this.setpolygon();
            this.ct.restore();
        }
    };
    //获取模式
    getMode() {
        return this.config.mode;
    }
    // 获取操作历史
    getHistory() {
        return this.config.history;
    }
    //历史跳转
    goto(index: number) {
        if (!this.config.history.List[index] || this.config.historyIndex == index) {
            return;
        }
        this.data = this.config.history.List[index].data;
        if (!this.data) {
            return
        }
        this.c.width = this.data.width;
        this.c.height = this.data.height;
        this.ct.putImageData(this.data, 0, 0);
        this.scaleCanvas(this.config.canvas.scale);
    }
    //添加历史
    addHistory(historyData: History) {
        this.config.history.push(historyData);
        if (this.config.history.length > this.config.historyMax) {
            this.config.history.shift();
        }
    }
    // 显示隐藏裁剪框
    showCuttingBox(show: boolean) {
        if (show) {
            this.config.elements.cutBox.style.display = 'block';
        } else {
            this.config.elements.cutBox.style.left = '-200px';
            this.config.elements.cutBox.style.width = '0';
            this.config.elements.cutBox.style.height = '0';
            this.config.elements.cutBox.style.display = 'none';
        }
    }
    // 设置画笔颜色
    setColor(color: string) {
        this.config.pen.color = color;
        var color_str = color;
        color_str = color_str.replace(/[rgba\(\)]/g, '');
        this.config.pen.colors = color_str.split(',');
    }
    // 获取画笔颜色
    getColor() {
        return this.config.pen.color
    }
    // 设置画笔大小
    setWidth(size: number) {
        this.config.pen.size = size;
    }
    //打开图片img对象
    openFile(file: any) {
        this.img = new Image();
        try {
            this.img.src = URL.createObjectURL(file)
        } catch (e) {
            console.log('必须是input的file对象！')
        }
        this.img.onload = () => {
            this.loadImg();
            this.ready();
        }
    }
    //加载图片
    loadImg() {
        this.ct.clearRect(0, 0, this.c.width, this.c.height);
        this.c.width = this.img.naturalWidth;
        this.c.height = this.img.naturalHeight;
        this.ct.drawImage(this.img, 0, 0);
        this.upData();
    }
    //获取原始图片
    getImg() {
        return this.img;
    }
    //获取保存数据
    get_save() {
        return this.data;
    }
    //采样点数据 ----处理--判断重复
    select_data(dd: any) {
        if (this.sample) {
            if (Math.abs(dd[0] - this.sample.data[0]) == 100) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }

    }
    //多边形选择
    polygon(x: number, y: number, type: number) {
        if (this.config.mode === 11 && type == 1) {
            this.ct.arc(x, y, this.config.pen.size / 10 * 2, 0, Math.PI * 2);
            this.ct.fill();
            this.ct.lineTo(x, y);
            this.ct.stroke();
        } else if (this.config.mode === 11 && type == 3) {
            //结束选择

            this.ct.closePath();
            this.ct.save();
            //创建选区
            this.ct.clearRect(0, 0, this.c.width, this.c.height);//清除选择框
            this.ct.clip();
            this.getareadata(this.data);
            this.ct.restore();
            this.ct.clearRect(0, 0, this.c.width, this.c.height);//清除选择框
            this.ct.save();
            // this.redraw(this,this.data);//还原数据
            this.ct.clip();
            this.ct.fillStyle = 'rgba(255,255,255,0.8)';
            this.ct.fillRect(0, 0, this.c.width, this.c.height);//设置选择样式
            this.config.mode = 0;
            if (this.polygoncallback) {
                if (typeof this.polygoncallback === "function") {
                    this.polygoncallback();//回调函数
                } else {
                    console.log('多边形选区回调函数错误！');
                }

            }

        }
    };
    //设置 多边形选区 样式
    setpolygon() {
        this.n_data = this.ct.getImageData(0, 0, this.c.width, this.c.height);
        for (var i = 0; i < this.n_data.data.length; i += 4) {
            //选择区
            if (this.n_data.data[i] === this.selectdata.data[i] && this.n_data.data[i + 1] === this.selectdata.data[i + 1] && this.n_data.data[i + 2] === this.selectdata.data[i + 2] && this.n_data.data[i + 3] === this.selectdata.data[i + 3]) {
                if (this.selectdata.data[i] != 0 && this.selectdata.data[i + 1] != 0 && this.selectdata.data[i + 2] != 0 && this.selectdata.data[i + 3] != 0) {
                    this.n_data.data[i] = 255;
                    this.n_data.data[i + 1] = 255;
                    this.n_data.data[i + 2] = 0;
                    this.n_data.data[i + 3] = 125;

                }
            }
        }
        this.select_type = 1;
        this.selectAnimate();
    };
    //设置选区---魔术棒工具--默认只选择 闭合区间
    wand(sx: number, sy: number) {
        this.select_type = 0;
        this.stopAnimate();
        this.o_data = this.ct.getImageData(0, 0, this.c.width, this.c.height);
        var dat = this.ct.getImageData(sx, sy, 1, 1);//采集数据
        if (this.select_data(dat)) {
            return [];
        } else {
            this.sample = dat;
        }
        this.n_data = this.ct.getImageData(0, 0, this.c.width, this.c.height);
        for (var i = 0; i < this.data.data.length; i += 4) {
            //选择区
            if (Math.abs(this.n_data.data[i] - dat.data[0]) <= this.alw && Math.abs(this.n_data.data[i + 1] - dat.data[1]) <= this.alw && Math.abs(this.n_data.data[i + 2] - dat.data[2]) <= this.alw && Math.abs(this.n_data.data[i + 3] - dat.data[3]) <= this.alw) {
                if (this.n_data.data[i + 3] > 150) {
                    this.n_data.data[i + 3] = dat.data[3] - 100;
                } else {
                    this.n_data.data[i + 3] = dat.data[3] + 100;
                }
                if (this.n_data.data[i + 3] != 0) {
                    this.n_data.data[i + 1] = dat.data[0];
                    this.n_data.data[i + 2] = dat.data[1];
                    this.n_data.data[i + 4] = dat.data[2];
                } else {
                    this.n_data.data[i] = 100;
                    this.n_data.data[i + 1] = 100;
                    this.n_data.data[i + 4] = 100;
                }
                var rc = 150;
                if (Math.abs(this.n_data.data[i] - 255) < rc && Math.abs(this.n_data.data[i + 1] - 255) < rc && Math.abs(this.n_data.data[i + 2] - 255) < rc && Math.abs(this.n_data.data[i + 3] - 255) < rc) {
                    this.n_data.data[i + 0] = 255;
                    this.n_data.data[i + 1] = 0;
                    this.n_data.data[i + 2] = 0;
                    this.n_data.data[i + 2] = 200;
                }
            }
            //非选择区
        }
        this.selectAnimate();
    }
    // 选区闪烁提示
    selectAnimate() {
        var toggle = true;
        this.select = true;
        this.animation = setInterval(() => {
            if (toggle) {
                this.ct.putImageData(this.n_data, 0, 0);
                toggle = false;
            } else {
                this.ct.putImageData(this.data, 0, 0);
                toggle = true;
            }
            // console.log('选择')
        }, 500);
    }
    // 选区闪烁提示关闭
    stopAnimate() {
        clearInterval(this.animation);
        this.select = false;
        this.ct.putImageData(this.data, 0, 0);
    }
    //设置容差
    setAlw(a: any) {
        this.alw = a;
    }
    //重置画布
    reMake(a: any) {
        this.data = this.o_data;
        this.config.history.clear();
        this.addHistory(new History('开始', this.data));
        this.goto(0);
    }
    //缩放画布
    scaleCanvas(size: number) {
        var cw = this.c.width;
        var ch = this.c.height;
        if (this.config.canvas.scale) {
            this.config.canvas.scale = size;
        } else if (this) {
            this.config.canvas.scale = size;
        }
        this.c.style.width = cw * (size / 100) + "px";
        this.c.style.height = ch * (size / 100) + "px";
    }
    //删除选区
    clearSelect() {

        if (this.lastmode === 11 && this.select_type === 1) {
            //多边形选择
            if (this.selectdata) {
                this.stopAnimate();
                this.upData();
                this.addHistory(new History('删除', this.data))
                var dat = this.sample;
                for (var i = 0; i < this.data.data.length; i += 4) {
                    //选择区
                    if (this.data.data[i] === this.selectdata.data[i] && this.data.data[i + 1] === this.selectdata.data[i + 1] && this.data.data[i + 2] === this.selectdata.data[i + 2] && this.data.data[i + 3] === this.selectdata.data[i + 3]) {
                        this.data.data[i] = 0;
                        this.data.data[i + 1] = 0;
                        this.data.data[i + 2] = 0;
                        this.data.data[i + 3] = 0;
                    }
                }
                this.ct.putImageData(this.data, 0, 0);
                this.ct.restore();
            }
        } else {
            //颜色选择
            if (this.select && this.sample) {
                this.stopAnimate();
                this.upData();
                this.addHistory(new History('删除', this.data))
                var dat = this.sample;
                for (var i = 0; i < this.data.data.length; i += 4) {
                    //选择区
                    if (Math.abs(this.data.data[i] - dat.data[0]) <= this.alw && Math.abs(this.data.data[i + 1] - dat.data[1]) <= this.alw && Math.abs(this.data.data[i + 2] - dat.data[2]) <= this.alw && Math.abs(this.data.data[i + 3] - dat.data[3]) <= this.alw) {
                        this.data.data[i] = 0;
                        this.data.data[i + 1] = 0;
                        this.data.data[i + 2] = 0;
                        this.data.data[i + 3] = 0;
                    }
                }
                this.ct.putImageData(this.data, 0, 0);
            }

        }
    }
    //填充选区
    fillSelect() {
        var color = this.config.pen.colors;

        if (this.select && this.select_type === 1 && this.selectdata) {

            //多边形选择
            if (this.selectdata) {
                this.stopAnimate();
                this.upData();
                this.addHistory(new History('填充', this.data))
                var dat = this.sample;
                for (var i = 0; i < this.data.data.length; i += 4) {
                    //选择区
                    if ((this.selectdata.data[i] != 0 && this.selectdata.data[i + 1] != 0 && this.selectdata.data[i + 2] != 0 && this.selectdata.data[i + 3] != 0) && (this.data.data[i] === this.selectdata.data[i] && this.data.data[i + 1] === this.selectdata.data[i + 1] && this.data.data[i + 2] === this.selectdata.data[i + 2] && this.data.data[i + 3] === this.selectdata.data[i + 3])) {
                        this.data.data[i] = color[0];
                        this.data.data[i + 1] = color[1];
                        this.data.data[i + 2] = color[2];
                        this.data.data[i + 3] = parseInt(color[3]) * 255;
                    }
                }
                this.ct.putImageData(this.data, 0, 0);
                this.ct.restore();
            }
        } else {
            //颜色选择
            if (this.select && this.sample) {
                this.stopAnimate();
                this.upData();
                this.addHistory(new History('填充', this.data))
                var dat = this.sample;
                for (var i = 0; i < this.data.data.length; i += 4) {
                    //选择区
                    if (Math.abs(this.data.data[i] - dat.data[0]) <= this.alw && Math.abs(this.data.data[i + 1] - dat.data[1]) <= this.alw && Math.abs(this.data.data[i + 2] - dat.data[2]) <= this.alw && Math.abs(this.data.data[i + 3] - dat.data[3]) <= this.alw) {
                        this.data.data[i] = color[0];
                        this.data.data[i + 1] = color[1];
                        this.data.data[i + 2] = color[2];
                        this.data.data[i + 3] = parseInt(color[3]) * 255;
                    }
                }
                this.ct.putImageData(this.data, 0, 0);
            }

        }
    }
    //更新标准data数据
    upData() {
        if (this.data) {
            this.data = this.ct.getImageData(0, 0, this.c.width, this.c.height);
            this.ct.putImageData(this.data, 0, 0);
        }
    }
    //调节颜色
    colorControl(color: string[]) {
        if (this.select && this.select_type === 1) {
            //多边形选择
            if (this.selectdata) {
                this.stopAnimate();
                this.upData();
                this.addHistory(new History('调色', this.data))
                var dat = this.sample;
                for (var i = 0; i < this.data.data.length; i += 4) {
                    //选择区
                    if (this.data.data[i] === this.selectdata.data[i] && this.data.data[i + 1] === this.selectdata.data[i + 1] && this.data.data[i + 2] === this.selectdata.data[i + 2] && this.data.data[i + 3] === this.selectdata.data[i + 3]) {
                        this.data.data[i] = color[0];
                        this.data.data[i + 1] = color[1];
                        this.data.data[i + 2] = color[2];
                        // this.data.data[i+3]=0;
                    }
                }
                this.ct.putImageData(this.data, 0, 0);
                this.ct.restore();
            }
        }
        else {
            if (this.select && this.sample) {
                this.stopAnimate();
                this.upData();
                this.addHistory(new History('调色', this.data))
                var dat = this.sample;
                for (var i = 0; i < this.data.data.length; i += 4) {
                    //选择区
                    if (Math.abs(this.data.data[i] - dat.data[0]) <= this.alw && Math.abs(this.data.data[i + 1] - dat.data[1]) <= this.alw && Math.abs(this.data.data[i + 2] - dat.data[2]) <= this.alw && Math.abs(this.data.data[i + 3] - dat.data[3]) <= this.alw) {
                        this.data.data[i] = color[0];
                        this.data.data[i + 1] = color[1];
                        this.data.data[i + 2] = color[2];
                        this.data.data[i + 3] = color[3];
                    }
                }
                this.ct.putImageData(this.data, 0, 0);
            }

        }

    }
    //输出 JPEG 格式
    save_jpg() {
        var img_data = this.ct.getImageData(0, 0, this.c.width, this.c.height);
        for (var i = 0; i < img_data.data.length; i += 4) {
            //选择区
            if (img_data.data[i + 3] == 0) {
                img_data.data[i] = 255;
                img_data.data[i + 1] = 255;
                img_data.data[i + 2] = 255;
                img_data.data[i + 3] = 255;
            }
        }
        return img_data;
    }
    //裁剪 ----图片
    imgCutting(x: number, y: number, w: number, h: number) {
        this.data = this.ct.getImageData(x, y, w, h);
        this.c.width = w;
        this.c.height = h;
        this.c.style.left = 0;
        this.c.style.top = 0;
        this.ct.putImageData(this.data, 0, 0);
    }
    //移动画布
    moveCanvas(x: number, y: number) {
        this.c.style.left = x + "px";
        this.c.style.top = y + "px";
    }
    // 鼠标移动执行操作
    mouseWork = ({
        //涂鸦
        draw: (x: number, y: number) => {
            if (this.config.mouse.status == 0) {
                this.ct.fillStyle = this.config.pen.color;
                this.ct.fillRect(x / (this.config.canvas.scale / 100), y / (this.config.canvas.scale / 100), this.config.pen.size, this.config.pen.size);
            }
        },
        //铅笔
        pencil: (x: number, y: number) => {
            if (this.config.mouse.status == 0) {

                this.ct.lineTo(x / (this.config.canvas.scale / 100), y / (this.config.canvas.scale / 100));
                this.ct.stroke();
            }
        },
        //铅笔抠图
        pencut: (x: number, y: number) => {
            if (this.config.mouse.status == 0) {
                this.ct.lineTo(x / (this.config.canvas.scale / 100), y / (this.config.canvas.scale / 100));
                this.ct.stroke();
            }
        },
        //橡皮擦
        clear: (x: number, y: number) => {
            if (this.config.mouse.status == 0) {
                this.ct.clearRect(x / (this.config.canvas.scale / 100), y / (this.config.canvas.scale / 100), this.config.pen.size, this.config.pen.size);
            }
        },
        //颜色拾取
        pickercolor: (x: number, y: number) => {
            if (this.config.mouse.status == 0) {
                var color = this.ct.getImageData(x / (this.config.canvas.scale / 100), y / (this.config.canvas.scale / 100), 1, 1).data;
                this.setColor('rgba(' + color[0] + ',' + color[1] + ',' + color[2] + ',' + color[3]);
                if (this.pickercallback) {
                    this.pickercallback(false);
                }
            }
        },
        //裁剪过程
        cutting: (x: number, y: number) => {
            if (this.config.mouse.status == 0) {
                var ww = Math.abs(this.config.mouse.down.x - x);
                var hh = Math.abs(this.config.mouse.down.y - y);
                if (this.config.mouse.down.x < x) {
                    if (this.config.mouse.down.y < y) {
                        this.config.elements.cutBox.style.left = this.config.mouse.down.x + "px";
                        this.config.elements.cutBox.style.top = this.config.mouse.down.y + "px";
                        this.config.elements.cutBox.style.width = ww + "px";
                        this.config.elements.cutBox.style.height = hh + "px";
                    } else {
                        this.config.elements.cutBox.style.left = this.config.mouse.down.x + "px";
                        this.config.elements.cutBox.style.top = y + "px";
                        this.config.elements.cutBox.style.width = ww + "px";
                        this.config.elements.cutBox.style.height = hh + "px";
                    }
                } else {
                    if (this.config.mouse.down.y < y) {
                        this.config.elements.cutBox.style.left = x + "px";
                        this.config.elements.cutBox.style.top = this.config.mouse.down.y + "px";
                        this.config.elements.cutBox.style.width = ww + "px";
                        this.config.elements.cutBox.style.height = hh + "px";
                    } else {
                        this.config.elements.cutBox.style.left = x + "px";
                        this.config.elements.cutBox.style.top = y + "px";
                        this.config.elements.cutBox.style.width = ww + "px";
                        this.config.elements.cutBox.style.height = hh + "px";
                    }
                }
            }
        },
        // 移动裁剪框
        moveCuttingBox: (x: number, y: number,) => {
            if (this.config.mouse.cutBox.status == 0) {
                var rx = parseInt(this.c.style.left);
                var ry = parseInt(this.c.style.top);
                if (x + this.config.elements.cutBox.offsetWidth <= this.c.offsetWidth + rx && x >= rx) {
                    this.config.elements.cutBox.style.left = x + 'px';

                }
                if (y + this.config.elements.cutBox.offsetHeight <= this.c.offsetHeight + ry && y >= ry) {
                    this.config.elements.cutBox.style.top = y + 'px';
                }
            }
        },

        // 移动画布
        moveCanvas: (x: number, y: number) => {
            if (this.config.mouse.status == 0) {
                this.c.style.left = x + "px";
                this.c.style.top = y + "px";
            }
        },
    })
    pencutdata() {
        this.ct.closePath();
        this.ct.stroke();
        this.ct.save();
        this.ct.clip();
        this.ct.clearRect(0, 0, this.c.width, this.c.height);
        this.ct.restore();
    };
}
(<any>window).CanvasPs = CanvasPs