/**
 * 编辑器配置
 */
const config = {
	//菜单栏工具
	tools: [
		{
			title: '打开图片',//标题
			icon: 'assets/imgs/folder.png',//图标
			action: 'controller.openfile()'//事件
		},
		{
			title: '保存图片',//标题
			icon: 'assets/imgs/save.png',//图标
			action: 'controller.svaeimg()'//事件
		},
		{
			title: '取消选择',//标题
			icon: 'assets/imgs/Pointer.png',//图标
			action: 'controller.cancel()'//事件
		},
		{
			title: '多边形选择',//标题
			icon: 'assets/imgs/polygon.svg',//图标
			action: 'controller.startpolygon(this)'//事件
		},
		{
			title: '删除选择',//标题
			icon: 'assets/imgs/emptytrash.png',//图标
			action: 'controller.del_select()'//事件
		},
		{
			title: '色彩平衡',//标题
			icon: 'assets/imgs/Color.png',//图标
			action: 'controller.opencolorwindow(this)'//事件
		},
		{
			title: '裁剪',//标题
			icon: 'assets/imgs/cut.svg',//图标
			action: 'controller.startcut(this)'//事件
		},
		{
			title: '移动画布',//标题
			icon: 'assets/imgs/move.svg',//图标
			action: 'controller.movecanvas(this)'//事件
		},
		{
			title: '重置画布',//标题
			icon: 'assets/imgs/Restore.png',//图标
			action: 'controller.resetcavans()'//事件
		},
		{
			title: '画笔',//标题
			icon: 'assets/imgs/paintbrush.svg',//图标
			action: 'controller.startdraw(this)'//事件
		},
		{
			title: '文本',
			icon: 'assets/imgs/text.svg',
			action: 'controller.draw_text(this)'
		},
		{
			title: '拾色器',//标题
			icon: 'assets/imgs/colorpicker.png',//图标
			action: 'controller.pickercolor(this)'//事件
		},
		{
			title: '铅笔',//标题
			icon: 'assets/imgs/pencil.svg',//图标
			action: 'controller.startpencil(this)'//事件
		},
		{
			title: '填充',//标题
			icon: 'assets/imgs/fill.png',//图标
			action: 'controller.fillcolor()'//事件
		},
		{
			title: '擦除',//标题
			icon: 'assets/imgs/eraser.svg',//图标
			action: 'controller.startclaer(this)'//事件
		},
		{
			title: '画笔抠图',//标题
			icon: 'assets/imgs/pen.svg',//图标
			action: 'controller.pencut(this)'//事件
		},
		{
			title: '帮助',//标题
			icon: 'assets/imgs/help.png',//图标
			action: 'controller.helper()'//事件
		},
	],
	tools_control: [
		{
			title: '容差',
			edit_type: 'input',
			value: '0',
			event: 'controller.settolerance(this)',
			click: ''
		},
		{
			title: '笔刷大小',
			edit_type: 'input',
			value: '10',
			event: 'controller.setpensize(this)',
			click: ''
		},
		{
			title: '笔刷颜色',
			edit_type: 'color',
			value: '',
			event: 'controller.changecolor(this)',
			click: 'controller.selectcolor(this)'
		},
		{
			title: '显示比例',
			edit_type: 'input',
			value: '100',
			event: 'controller.scalecavans(this)',
			click: ''
		},
	]
}
export default config